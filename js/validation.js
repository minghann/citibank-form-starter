$(function(){

	/* DOCUMENTATION

	Things to note:

	1. form name must be change according to campaign/landing page name.
	2. make sure each inputs (input, select or textarea) have the name attribute like
	<input name="fname" type="text">
	which later the script will append those attribute to kana form

	Validation.js attribute usage
	=================================================================================
	abcOnly                      : make the field to allow only alphabet characters
	numOnly                      : make the field to allow only numbers
	mandatory                    : make the field mandatory to be filled and cannot be empty
	email                        : make the field to validate if the email entered is a valid email
	maxlength                    : make the field to have certain length of characters and the length of the characters entered must not less than the specified length
	mobile                       : make the field to allow 7-8 characters length which will be used for the mobile suffix field
	data-placeholder="something" : act as a dynamic variable/placeholder used as name for a particular fields to prompt alert message

	*/

	var init_options = ['abcOnly','numOnly'];
	var options = ['mandatory','maxlength','email'];
	var all_validated = false;

	var onload_validations = { // validation before user click submit form

		abcOnly : function(that){
			$(that).keydown(function(event) {

			var evt = (evt) ? evt : event;
			var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
						((evt.which) ? evt.which : 0));
					if (charCode > 31 && (charCode < 65 || charCode > 90) &&
						(charCode < 97 || charCode > 122) && (charCode !=32 )) {

						return false;
				}
				return true;
			});

		},
		numOnly : function(that){
			
			$(that).keydown(function(event){
				var e = event || window.event;
				var key = e.keyCode || e.which;

				if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
				    // numbers   
				    key >= 48 && key <= 57 ||
				    // Numeric keypad
				    key >= 96 && key <= 105 ||
				    // Backspace and Tabfand Enter
				    key == 8 || key == 9 || key == 13 ||
				    // Home and End
				    key == 35 || key == 36 ||
				    // left and right arrows
				    key == 37 || key == 39 || key == 187 ||
				    // Del and Ins
				    key == 46 || key == 45) {
				        // input is VALID
				}
				else {
				        // input is INVALID
				        e.returnValue = false;
				        if (e.preventDefault) e.preventDefault();
				    }
				});
		}
	}

	var validation_events = { // validation layer upon user click submit form
		mandatory : function(that){
			if(!$(that).val() && $(that).data('placeholder')){
				alert('Please fill in ' + $(that).data('placeholder') + '.');
				$(that).focus();
				all_validated = false;
				return false;
			}
			all_validated = true;
			return true;
		},

		maxlength : function(that){

			if($(that).is("[mobile]")){

				if($(that).val().length < ($(that).attr('maxlength') - 1)){
					alert('Please fill in a valid ' + $(that).data('placeholder') + '.');
					$(that).focus();
					all_validated = false;
					return false;
				}

			}else{

				if($(that).val().length < $(that).attr('maxlength')){
					alert('Please fill in a valid ' + $(that).data('placeholder') + '.');
					$(that).focus();
					all_validated = false;
					return false;
				}
			}

			all_validated = true;
			return true;
		},

		email : function(that){
			var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if ($(that).val() == '' || !regex.test($(that).val()))
			{
				alert('Please make sure the '+ $(that).data('placeholder') +' is valid.');
				$(that).focus();
				all_validated = false;
				return false;
			}
			all_validated = true;
			return true;
		}
	}
	

	function init(){

		//disable space on first character
		$('body').on('keydown', 'input,select,textarea', function(e) {
			if (e.which === 32 &&  e.target.selectionStart === 0) {
				return false;
			}
		});

		//disable cut, copy and paste
		$('input,select,textarea').bind("cut copy paste", function(e) {
			e.preventDefault();
		});

		var continue_validate = true;
		$('input,select,textarea').each(function(e){
			var self = $(this);

			if(continue_validate != false){
				$.each(init_options, function(i, val){
					var attr = self.attr(val); // option array
					if (typeof attr !== typeof undefined && attr !== false) {
						continue_validate = onload_validations[val](self);						
					}
				});
			}
		})
	}

	init();

	function submit_validation(callback){
		var continue_validate = true;
		var inputs = $('input:visible,select:visible,textarea:visible');
		inputs.each(function(e){
			var self = $(this);

			if(continue_validate != false){

				$.each(options, function(i, val){

					var attr = self.attr(val);
					if (typeof attr !== typeof undefined && attr !== false) {
						
						continue_validate = validation_events[val](self);
						if(!continue_validate){
						    return false;
						}
					}
				});
			}
		});
	}

	function submit_form(f){

		var subject = $(f).attr('name');

		var f = document[subject];
		
		//build required data for kana form
		var new_paramOrder = ',';
		var new_paramAlias = ',';
		var inputs = $('input, textarea, select').not(':input[type=hidden]');
		inputs.each(function(e){

			var self = $(this);

			if(!self.val()){
				f[self.attr('name')].value = 'N/A';
			}
			new_paramOrder += self.attr('name') + ',';
			new_paramAlias += self.data('placeholder') + ',';
		});

		f['_paramOrder'].value += trimCommas(new_paramOrder);
		f['_paramAlias'].value += trimCommas(new_paramAlias);

		f.subject.value = "CBOL_" + subject + multigup('ecid','icid');

		if ((self.location+'').indexOf("citibank.com.my/")>0) f.action = "/MYGCB/apfa/genfm/ProcessForm.do";
		else f.action = "/kanatest.php";
		
		window.open('thank-you.html','thankyou','scrollbars=yes,width=1000,height=650,resizable=yes');
		f.target= "_self";
		pageload1=1;
		f.submit();
	}

	$('#submit_form').click(function(e){
		e.preventDefault();
		submit_validation();

		if(all_validated){
			submit_form($(this).closest('form'));
		}
	});

	function toTitleCase(str){	
		if(str){
			return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		}
	}

	function trimCommas(str){
		return str.substring(0, str.length - 1);
	}

	/*citibank function*/
	function multigup(sname1,sname2)
	{
		var result = '';

		if(gup(sname1) == '')
		{
			result = gup(sname2);
		}
		else
		{
			result = gup(sname1);
		}

		var n = result.split("_");

		if(n.length < 4)
		{
			return "";
		}

		return n[0] + "_" + n[1] + "_" + n[2] + "_" + n[3];
	}

	function gup(sname) {  
		sname = sname.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
		var regexS = "[\\?&]"+sname+"=([^&#]*)";  
		var regex = new RegExp( regexS );  
		var results = regex.exec( window.location.href );  
		if( results == null )   
			return "";  
		else    
			return results[1];
	}

});